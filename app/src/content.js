// Inform the background page that 
// this tab should have a page-action
chrome.runtime.sendMessage({
  from: 'content',
  action: 'showPageAction'
});

// Listen for messages from the popup
chrome.runtime.onMessage.addListener((msg, sender, response) => {
  // First, validate the message's structure
  if ((msg.from === 'popup') && (msg.action === 'DOMInfo')) {
    // Collect the necessary data

    let header = document.querySelector(".course-section-header h1");
    let headerText = (header) ? header.textContent : '';

    let domInfo = {
      course: headerText.slice(0, headerText.indexOf("-")).toLowerCase().trim(),
    };

    // Directly respond to the sender (popup), 
    // through the specified callback */
    response(domInfo);
  }
});
