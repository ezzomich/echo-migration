/**
 * Save Options
 */
function saveOptions() {
  // Get the values from the form
  let enableHD = (document.getElementById('hdenable').checked) ? true : false;
  let enableJson = (document.getElementById('jsonenable').checked) ? true : false;
  let enableXml = (document.getElementById('xmlenable').checked) ? true : false;
  let defaultOwner = document.getElementById('defaultowner').value;
  let defaultCategory = document.getElementById('categoryid').value;
  let conversionProfileId = document.getElementById('conversionprofileid').value;
  let kalturaPartnerId = document.getElementById('kpartnerid').value;
  let kalturaSecret = document.getElementById('ksecret').value;

  // Store the values in chrome sync
  chrome.storage.local.set({
    enableHD: enableHD,
    enableJson: enableJson,
    enableXml: enableXml,
    defaultOwner: defaultOwner,
    defaultCategory: defaultCategory,
    conversionProfileId: conversionProfileId,
    kalturaPartnerId: kalturaPartnerId,
    kalturaSecret: kalturaSecret
  }, _ => {
    // Update status to let user know options were saved.
    displayFinished('Saved Settings!');
  });
}

/**
 * Resets Options to default sttings
 */
function resetOptions() {
  // Set Default Chrome Values
  chrome.storage.local.set({
    enableHD: true,
    enableJson: false,
    enableXml: true,
    defaultOwner: '',
    defaultCategory: '31887141',
    conversionProfileId: '',
    kalturaPartnerId: '',
    kalturaSecret: ''
  }, _ => {
    // Update status to let user know options were saved.
    restoreOptions();
    displayFinished('Resetted Settings to Default!');
  });
}

/**
 * reloads currently saved options into values
 */
function restoreOptions() {
  // Get the values from chrome sync, use defaults if not available
  chrome.storage.local.get({
    enableHD: true,
    enableJson: false,
    enableXml: true,
    defaultOwner: '',
    defaultCategory: '31887141',
    conversionProfileId: '',
    kalturaPartnerId: '',
    kalturaSecret: ''
  }, (items) => {
    if (items.enableHD) {
      document.querySelector('#hdenable').parentNode.MaterialCheckbox.check();
    } else {
      document.querySelector('#hdenable').parentNode.MaterialCheckbox.uncheck();
    }

    if (items.enableJson) {
      document.querySelector('#jsonenable').parentNode.MaterialCheckbox.check();
    } else {
      document.querySelector('#jsonenable').parentNode.MaterialCheckbox.uncheck();
    }

    if (items.enableXml) {
      document.querySelector('#xmlenable').parentNode.MaterialCheckbox.check();
    } else {
      document.querySelector('#xmlenable').parentNode.MaterialCheckbox.uncheck();
    }

    document.querySelector('#defaultowner').parentNode.MaterialTextfield.change(items.defaultOwner);
    document.querySelector('#categoryid').parentNode.MaterialTextfield.change(items.defaultCategory);
    document.querySelector('#conversionprofileid').parentNode.MaterialTextfield.change(items.conversionProfileId);
    document.querySelector('#kpartnerid').parentNode.MaterialTextfield.change(items.kalturaPartnerId);
    document.querySelector('#ksecret').parentNode.MaterialTextfield.change(items.kalturaSecret);
  });
}

/**
 * Displays snackback message
 * @param {string} showMessage The message to display in the snackbar
 */
function displayFinished(showMessage) {
  var snackbarContainer = document.querySelector('#snackbar');
  var data = { message: showMessage };
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

document.addEventListener('DOMContentLoaded', _ => {
  // Wait for mdl and the run restore
  document.querySelector('.mdl-layout').addEventListener('mdl-componentupgraded', function () {
    restoreOptions();
  });
  document.getElementById('saveOptions').addEventListener('click', saveOptions);
  document.getElementById('resetOptions').addEventListener('click', resetOptions);
});
