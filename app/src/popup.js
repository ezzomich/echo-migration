let mediaLessons = undefined;
const shouldDownload = true;
let downloadHD = true;
let downloadables = [];
let filtered = [];
let echo360Domain = '';

const echo360BaseURIs = [
  'echo360.org.au',
  'echo360.org.uk',
  'echo360.org'
]

/**
 * Determines if a lesson is downloadable
 * @param {Object} param0 The downloadable object from Echo360
 * @returns {boolean} Whether the item is downloadable
 */
function canDownload({ lesson }) {
  return (lesson.isFuture === false && lesson.hasAvailableVideo === true && lesson.video != null && lesson.video.media && lesson.video.media.media) ? true : false;
}

/**
 * Returns filename for video to be displayed in the pop-up list
 * @param {Object} param0 The downloadable object from Echo360
 * @returns {string} The name to show in the pop-up list
 */
function getVideoFileName({ lesson }) {
  const { updatedAt } = lesson.video.media;
  return lesson.lesson.name + " - " + updatedAt.slice(0, updatedAt.indexOf("T"));
}

/**
 * Job of this function is to listen init mediaLessons once per click.
 * @param {XMLHttpRequest} xhrRequest An XHR request to process
 */
function webRequestOnComplete(xhrRequest) {
  console.log("Media Lessons obtained!");

  if (mediaLessons === undefined) {
    mediaLessons = xhrRequest;
    // Now perform the request again ourselves and download files.
    var getMediaLessonsRequest = new Request(mediaLessons.url, { method: 'GET' });
    fetch(
      getMediaLessonsRequest,
      {
        method: 'GET',
        credentials: 'include'
      })
      .then((getMediaLessonsResponse) => getMediaLessonsResponse.json())
      .then((getMediaLessonsJson) => {
        console.log(getMediaLessonsJson);
        downloadables = getMediaLessonsJson.data.filter((dataItem) => {
          return canDownload(dataItem);
        });

        // sort downloadables
        downloadables.sort((a, b) => {
          const nameA = getVideoFileName(a), nameB = getVideoFileName(b);
          if (nameA < nameB) return -1;
          else if (nameA == nameB) return 0;
          else return 1;
        });
        const lectureTable = document.getElementById("lectures");
        const lectureSelect = document.getElementById("lectureSelect");
        downloadables.forEach((downloadable) => {
          const option = document.createElement("option");
          option.defaultSelected = true;
          const name = getVideoFileName(downloadable);

          option.innerHTML = name;
          lectureSelect.appendChild(option);
        });

        var downloadButton = document.getElementById('download');
        downloadButton.disabled = false;
      }
      );
  }
}

/**
 * Sets up the pop-up
 */
function pageSetup() {
  // If we're not on an Echo page, then don't show the UI
  document.getElementById("versionLabel").innerText = chrome.runtime.getManifest().version;
  chrome.tabs.query({ currentWindow: true, active: true }, (tabs) => {
    var currentTab = tabs[0].url;
    console.log(currentTab);
    var domain = currentTab.match(/^[\w-]+:\/{2,}\[?([\w\.:-]+)\]?(?::[0-9]*)?/)[1];
    if (echo360BaseURIs.indexOf(domain) === -1) {
      document.getElementById("load").setAttribute("disabled", true);
      document.getElementById("downloadHD").setAttribute("disabled", true);
      document.getElementById("mainBlock").setAttribute("hidden", true);
      document.getElementById("invalidMsg").removeAttribute("hidden")
    }
    echo360Domain = domain;
  });

  // Wait for mdl and get default for HD checkbox and set initial state
  document.querySelector('#downloadHD').parentNode.addEventListener('mdl-componentupgraded', function () {
    chrome.storage.local.get({
      enableHD: true
    }, (items) => {
      if (items.enableHD) {
        document.querySelector('#downloadHD').parentNode.MaterialCheckbox.check();
      } else {
        document.querySelector('#downloadHD').parentNode.MaterialCheckbox.uncheck();
      }
    });
  });
}

document.addEventListener('DOMContentLoaded', _ => {
  //Connect to background script.
  const port = chrome.runtime.connect();

  // Add load button onclick. To refresh page to populate
  var loadButton = document.getElementById('load');
  pageSetup();

  loadButton.addEventListener('click', _ => {

    chrome.tabs.query({ currentWindow: true, active: true }, (tabs) => {

      var currentTab = tabs[0].url;
      var domain = currentTab.match(/^[\w-]+:\/{2,}\[?([\w\.:-]+)\]?(?::[0-9]*)?/)[1];

      downloadHD = (document.getElementById("downloadHD").checked) ? true : false;
      console.log("echo360loaded", echo360Domain)
      chrome.webRequest.onCompleted.addListener(webRequestOnComplete, { urls: [`*://${domain}/*/syllabus`] });

      chrome.tabs.getSelected(null, (tab) => {
        var code = 'window.location.reload();';
        chrome.tabs.executeScript(tab.id, { code: code });
      });

      chrome.tabs.sendMessage(tabs[0].id, { from: 'popup', action: 'DOMInfo' }, (items) => {
        // stick the course code in a hidden field and additional tags
        document.getElementById('courseId').value = items.course;
        document.querySelector('#additionalTags').parentNode.MaterialTextfield.change(document.getElementById('additionalTags').value + ', ' + items.course);
      });

    });
  }, false);

  // Add listener to open page options
  document.getElementById("optionsBtn").addEventListener('click', _ => {
    chrome.runtime.openOptionsPage();
  });

  // Add download button onclick.
  var downloadButton = document.getElementById('download');
  downloadButton.disabled = true;
  downloadButton.addEventListener('click', _ => {
    // Get data from form
    const lectureSelect = document.getElementById("lectureSelect");
    const options = lectureSelect.options;

    let selected = [];
    for (let i = 0; i < options.length; i++) {
      if (options[i].selected)
        selected.push(i);
    }

    // Using index as unique ID, since dates are not unique.
    let toDownload = [];
    for (let i = 0; i < downloadables.length; i++) {
      if (selected.indexOf(i) != -1)
        toDownload.push(downloadables[i]);
    }

    // Send data to background.js -- theres a bit of pre-processing happening here too.
    port.postMessage({
      'toDownload': toDownload,
      'downloadHD': (document.getElementById("downloadHD").checked) ? true : false,
      'ownerID': String(document.getElementById("ownerId").value).toLowerCase().trim(),
      'coEditors': Array.from(
        new Set(String(document.getElementById("coEditors").value).toLowerCase().split(/[\s,;]+/).map((item) => item.trim()))
      ),
      'coPublishers': Array.from(
        new Set(String(document.getElementById("coPublishers").value).toLowerCase().split(/[\s,;]+/).map((item) => item.trim()))
      ),
      'categories': Array.from(
        new Set(String(document.getElementById("categories").value).toLowerCase().split(/[\s,;]+/).map((item) => item.trim()))
      ),
      'additionalTags': String(document.getElementById("additionalTags").value).toLowerCase().split(/[,;]+/).map((item) => item.trim()),
      'courseId': document.getElementById('courseId').value,
      'echo360Domain': echo360Domain
    });

    // clear the selection
    //for (let i = 0; i < options.length; i++) {
    //  options[i].selected = false;
    //}
    mediaLessons = undefined;
    return;
  }, false);

}, false);
