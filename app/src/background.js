/**
 * Safely retrieves a path from an object
 * @param {string[]} path An array where each value represents part of the path
 * @param {Object} obj The object to traverse
 * @param {*} [defaultValue] The default value to return if the path does not exist
 * @returns {*} The value of the oject at path or defaultValue
 */
function getProperty(path, obj, defaultValue = null) {
  return path.reduce((xs, x) => (xs && xs[x]) ? xs[x] : defaultValue, obj);
}

/**
 * Returns a unquie file Id to be used as the base of each file
 * @param {Object} param0 The downloadable object from Echo360 
 * @param {number} timestamp A time stamp to append to the file id
 * @returns {string} A uniqe id for the video file
 */
function getVideoFileId({ lesson }, timestamp) {
  return [
    getProperty(["video", "media", "id"], lesson, 'lecture'),
    timestamp
  ].join('-');
}

/**
 * Gets the folder name to use when saving to computer
 * @param {Object} param0 The downloadable object from Echo360
 * @param {string} prefix The prefix to apply to the folder name
 * @returns {string} Folder name
 */
function getFolderName({ lesson }, prefix) {
  return [
    prefix,
    getProperty(["video", "published", "courseName"], lesson, 'Unknown Name'),
    getProperty(["video", "published", "termName"], lesson, 'Unknown Term')
  ].join(" - ");
}

/**
 * Returns a UNIX timestamp
 * @returns {number} A Unix Timestamp based on the current time
 */
function time() {
  return Math.floor(new Date().getTime() / 1000);
}

/**
 * A JS implementation of PHP's rand function
 * @param {number} [min] The lowest value to return
 * @param {number} [max] The highest value to return
 * @returns {number} A pseudo random value between min and max
 */
function rand(min, max) {
  min = min || 0;
  max = max || Number.MAX_SAFE_INTEGER;

  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * A JS implementation of PHP's explode function
 * @param {string} delimiter The boundary string.
 * @param {string} str The input string
 * @param {number} [limit] If set, the returned array will contain a maxium of limit elements with the last element containing the rest of the string
 * @returns {string[]} An array of string created by spliting on the boundaries formed by delimeter
 */
function explode(delimiter, str, limit) {
  const arr = str.split(delimiter);
  if (limit !== undefined && arr.length >= limit) {
    arr.push(arr.splice(limit - 1).join(delimiter));
  }
  return arr;
}

/**
 * A JS implementation of PHP's sha1
 * @param {string} str The input string
 * @param {boolean} [raw_output] If the optional raw_output is set to true, then the sha1 digest is instead returned in raw binary format with a length of 20, otherwise the returned value is a 40-character hexadecimal number.
 * @returns {string} Returns the sha1 hash as a string.
 */
function sha1(str, raw_output = false) {
  let md = forge.md.sha1.create();
  md.update(str);
  return (raw_output) ? md.digest().getBytes() : md.digest().toHex();
}

/**
 * AES encrypts a message with a key -- used for kaltura sessions
 * @param {string} key A key to encrypt the message with
 * @param {string} message The message to encrypt
 * @returns {string} The encrypted message
 */
function aesEncrypt(key, message) {
  // no need for an IV since we add a random string to the message anyway
  iv = "\0".repeat(16);
  key = sha1(key, true).substr(0, 16);

  let cipher = forge.cipher.createCipher('AES-CBC', key);
  cipher.mode.pad = false;
  cipher.start({ iv: iv });
  cipher.update(forge.util.createBuffer(message));
  cipher.finish();
  return cipher.output.getBytes();
}

/**
 * A JS implementation of Kalturas PHP GenerateSessionV2 Method
 * @param {string} adminSecretForSigning The Admin Secret for signing the KS
 * @param {string} userId The userId associated with the KS
 * @param {number} type The type of KS 0 for USER, 2 for ADMIN
 * @param {number} partnerId The partner ID the KS is issued under
 * @param {number} expiry A UNIX timestamp for when the KS should expire
 * @param {string} [privileges] An option string of privileges for the KS
 * @returns {string} The encoded KS
 */
function generateKalturaSession(adminSecretForSigning, userId, type, partnerId, expiry, privileges = '') {
  // build fields array
  let fields = {};

  let privilegeArray = explode(',', privileges);
  for (let i = 0; i < privilegeArray.length; i++) {
    privilegeArray[i] = privilegeArray[i].trim();
    if (!privilegeArray[i]) {
      continue;
    }
    if (privilegeArray[i] == '*') {
      privilegeArray[i] = 'all:*';
    }

    let splittedPrivilege = explode(':', privilegeArray[i], 2);
    if (splittedPrivilege.length > 1) {
      fields[splittedPrivilege[0]] = splittedPrivilege[1];
    } else {
      fields[splittedPrivilege[0]] = '';
    }
  }

  fields._e = time() + expiry;
  fields._t = type;
  fields._u = userId;

  let searchParameters = new URLSearchParams();
  Object.keys(fields).forEach((parameterName) => {
    searchParameters.append(parameterName, fields[parameterName]);
  });

  let fieldsStr = searchParameters.toString();

  let r = forge.random.getBytesSync(16);
  //  for (let j = 0; j < 16; j++) {
  //    r = r.concat(String.fromCharCode(rand(0, 255)));
  //  }
  fieldsStr = r.concat(fieldsStr);
  fieldsStr = sha1(fieldsStr, true).concat(fieldsStr);

  // encrypt and encode
  let encryptedFields = aesEncrypt(adminSecretForSigning, fieldsStr);
  let decodedKs = "v2|" + partnerId + '|' + encryptedFields;

  let encodedKs = forge.util.encode64(decodedKs);

  encodedKs = encodedKs.replace(/\+/g, '-');
  encodedKs = encodedKs.replace(/\//g, '_');

  return encodedKs;
}

/**
 * A helper method for generating XML nodes
 * @param {XMLDocument} doc An XMLDocument object from which a node will be created
 * @param {HTMLElement} parentNode The node at wich the new node will be attached
 * @param {string} newNodeName A string that specifies the tag name of the element to be created
 * @param {string|string[]} content The content to add, may be a string or an array of strings
 */
function addTextNode(doc, parentNode, newNodeName, content) {
  if (Array.isArray(content)) {
    // Doc fragment for nested elements
    let fragment = doc.createDocumentFragment();

    // create a node for each value
    content.forEach((value) => {
      if (value != "") {
        addTextNode(doc, fragment, newNodeName, value);
      }
    });

    // append the fragment to the node
    parentNode.appendChild(fragment);
  } else {
    if (content) {
      let newNode = doc.createElement(newNodeName);
      newNode.textContent = String(content);
      parentNode.appendChild(newNode);
    }
  }
}

/**
 * Makes an XML string describing an Echo360 lecture.
 * @param {Object} param0 The downloadable object from Echo360 
 * @param {string} fileName The unique filename base used for downloaded files 
 * @param {string} ownerID The ownerID for kaltura
 * @param {string[]} [coEditors] An array of CoEditor Ids for kaltura
 * @param {string[]} [coPublishers] An array of CoPublisher Ids for kaltura
 * @param {string[]} [categories] An array of category Ids for kaltura
 * @param {string[]} [additionalTags] An array of additional tags to apply to the entry
 * @param {boolean|string} [hasSecondary] Whether or not there's a secondary video for this entry
 * @param {boolean|string} [hasPresentation] Wheter or not a presentation is attached to this entry
 * @param {string} [conversionProfileId] A Conversion ProfileId to use
 * @param {string} [kalturaSession] A KS to be embedded in the XML
 * @returns {string} The XML
 */
function getXML({ lesson }, fileName, ownerID = '', coEditors = [], coPublishers = [], categories = [], additionalTags = [], hasSecondary = false, hasPresentation = false, conversionProfileId = '', kalturaSession = '') {
  // create XML root
  let parser = new DOMParser();
  let xml = '<?xml version="1.0"?><mrss xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" version="1.0"></mrss>'
  let doc = parser.parseFromString(xml, "text/xml");

  // Get mrss element
  let mrss = doc.getElementsByTagName("mrss")[0];

  // Create ks element
  if (kalturaSession != "") {
    addTextNode(doc, mrss, "ks", kalturaSession);
  }

  // Create channel element
  let channel = doc.createElement("channel");

  // Create items -- there should only be 2
  // The second item is null for now
  let item = doc.createElement("item");
  let item2 = null;

  // Create Action and add to item
  addTextNode(doc, item, "action", "add");

  // Create Type and add to item
  addTextNode(doc, item, "type", "1");

  // Create Reference ID -- use the filename base
  addTextNode(doc, item, "referenceId", fileName);

  // Create UserId and add to item
  addTextNode(doc, item, "userId", ownerID);

  // Create Co-Editors and add to item
  let entitledUsersEdit = doc.createElement("entitledUsersEdit");
  addTextNode(doc, entitledUsersEdit, 'user', coEditors);
  item.appendChild(entitledUsersEdit);

  // Create Co-Publishers and add to item
  let entitledUsersPublish = doc.createElement("entitledUsersPublish");
  addTextNode(doc, entitledUsersPublish, 'user', coPublishers);
  item.appendChild(entitledUsersPublish);

  // Create Name and add to item
  let createDate = new Date(getProperty(["video", "media", "createdAt"], lesson));
  addTextNode(doc, item, "name", getProperty(["video", "media", "name"], lesson, 'No Name')); // + " - " + createDate.toLocaleDateString("en-US"));

  // Create Description and add to item
  addTextNode(doc, item, "description", getProperty(["video", "media", "description"], lesson));

  // Create Tags and add to item
  let tags = doc.createElement("tags");
  // Add some stuff to additionalTags
  additionalTags.push(
    String(getProperty(["userSection", "sectionNumber"], lesson, '')).toLowerCase().trim(),
    String(getProperty(["video", "published", "termName"], lesson, '')).toLowerCase().trim(),
    String(getProperty(["video", "published", "courseName"], lesson, '')).toLowerCase().trim(),
    ...getProperty(["video", "media", "tags"], lesson, []).map((item) => item.toLowerCase().trim())
  );
  // Only get the unique stuff and add
  addTextNode(doc, tags, 'tag', Array.from(new Set(additionalTags)));
  item.appendChild(tags);

  // Create Categories and add to item
  let pubcategories = doc.createElement("categories");
  addTextNode(doc, pubcategories, 'categoryId', categories);
  item.appendChild(pubcategories);

  // Put a conversion profile id in the xml
  if (conversionProfileId != '') {
    addTextNode(doc, item, "conversionProfileId", conversionProfileId);
  }

  // Create Media and add to item
  let media = doc.createElement("media");
  addTextNode(doc, media, "mediaType", "1");
  item.appendChild(media);

  // Create Content and add to item
  let contentAssets = doc.createElement("contentAssets");
  let content = doc.createElement("content");
  let dropFolderFileContentResource = doc.createElement("dropFolderFileContentResource");
  dropFolderFileContentResource.setAttribute("filePath", '1_' + fileName + '.mp4');
  content.appendChild(dropFolderFileContentResource);
  contentAssets.appendChild(content);
  item.appendChild(contentAssets);

  // Add Recording Date to Custom Field
  let customDataItems = doc.createElement("customDataItems");
  let customDateData = doc.createElement("customData");
  customDateData.setAttribute("metadataProfile", "migration");
  let xmlData = doc.createElement("xmlData");
  let metadata = doc.createElement("metadata");
  addTextNode(doc, metadata, "RecordingDateTime", Math.floor(createDate.getTime()/1000));
  xmlData.appendChild(metadata);
  customDateData.appendChild(xmlData);

  // Add sync status to custom field
  let customSyncData = customDateData.cloneNode(true);
  customSyncData.setAttribute("metadataProfile", "apinotificationssyncdata");
  let syncStatus = doc.createElement("SyncStatus");
  syncStatus.textContent = 'Sync Done';
  let metadataNode = customSyncData.getElementsByTagName("xmlData")[0].childNodes[0];
  metadataNode.replaceChild(syncStatus, metadataNode.childNodes[0]);

  customDataItems.appendChild(customSyncData);
  customDataItems.appendChild(customDateData);
  item.appendChild(customDataItems);

  // TAKE A BREAK FROM ITEM 1 for a SEC
  // check if there's a second video
  if (hasSecondary) {
    // Create item2 -- clone of item
    item2 = item.cloneNode(true);

    // replace the reference Id
    let parentReferenceId = doc.createElement("parentReferenceId");
    parentReferenceId.textContent = fileName;
    item2.replaceChild(parentReferenceId, item2.getElementsByTagName("referenceId")[0]);

    // remove extra stuff
    item2.removeChild(item2.getElementsByTagName("entitledUsersEdit")[0]);
    item2.removeChild(item2.getElementsByTagName("entitledUsersPublish")[0]);
    item2.removeChild(item2.getElementsByTagName("tags")[0]);
    item2.removeChild(item2.getElementsByTagName("categories")[0]);

    // change the filepath attribute on dropFolderFileContentResource
    item2.getElementsByTagName("contentAssets")[0].childNodes[0].childNodes[0].setAttribute(
      "filePath",
      '2_' + fileName + '.mp4'
    );
  }

  // RESUME ITEM 1
  // Add slides if they exist
  if (hasPresentation) {
    let attachments = doc.createElement("attachments");
    let attachment = doc.createElement("attachment");
    attachment.setAttribute("format", 3);

    dropFolderFileContentResource = doc.createElement("dropFolderFileContentResource");
    dropFolderFileContentResource.setAttribute(
      "filePath",
      fileName + '.' + getProperty(["slideDeck", "media", "media", "originalFile", "ext"], lesson, 'ppt')
    );
    attachment.appendChild(dropFolderFileContentResource);

    addTextNode(doc, attachment, "filename", getProperty(["slideDeck", "media", "media", "originalFile", "name"], lesson));
    addTextNode(doc, attachment, "title", getProperty(["slideDeck", "media", "name"], lesson));
    attachments.appendChild(attachment);
    item.appendChild(attachments);
  }

  //  if (/*Something to test for captions*/) {
  //    let subtitles = doc.createElement("subTitles");
  //    let subtitle = doc.createElement("subTitle");
  //    subtitle.setAttribute("isDefault", "true");
  //    subtitle.setAttribute("format", "1");
  //    subtitle.setAttribute("lang", "English (American)");

  //    dropFolderFileContentResource = doc.createElement("dropFolderFileContentResource");
  //    dropFolderFileContentResource.setAttribute("filePath", fileName + '.srt');
  //    subtitle.appendChild(dropFolderFileContentResource);

  //    subtitles.appendChild(subtitle);
  //    item.appendChild(subtitles);
  //  }

  // Add item to channel
  channel.appendChild(item);

  // Add item2 if it exists
  if (item2 !== null) {
    channel.appendChild(item2);
  }

  // Add channel to doc
  mrss.appendChild(channel);

  let serializer = new XMLSerializer();
  return serializer.serializeToString(doc);
}

/**
 * Gets the primay download link
 * @param {Object} param0 The downloadable object from Echo360
 * @param {boolean} [downloadHD] Whether we should get the HD or SD version
 * @returns {string} The S3 URL to the download
 */
function getPrimaryDownloadLink({ lesson }, downloadHD = true) {
  const { primaryFiles } = lesson.video.media.media.current;

  if (downloadHD) {
    const { s3Url, width, height, size } = primaryFiles[1];
    return s3Url;
  } else {
    const { s3Url, width, height, size } = primaryFiles[0];
    return s3Url;
  }
}

/**
 * Gets the secondary download link
 * @param {Object} param0 The downloadable object from Echo360
 * @param {boolean} [downloadHD] Whether we should get the HD or SD version
 * @returns {false|string} The S3 URL to the download or false on failure
 */
function getSecondaryDownloadLink({ lesson }, downloadHD = true) {
  const { secondaryFiles } = lesson.video.media.media.current;

  if (!Array.isArray(secondaryFiles) || secondaryFiles.length == 0) {
    return false;
  }

  if (downloadHD) {
    const { s3Url, width, height, size } = secondaryFiles[1];
    return s3Url;
  } else {
    const { s3Url, width, height, size } = secondaryFiles[0];
    return s3Url;
  }
}

/**
 * Gets the download link for a slideshow
 * @param {Object} param0 The downloadable object from Echo360
 * @returns {false|string} The S3 URL to the download or false on failure
 */
function getPresentationDownloadLink({ lesson }) {
  if (getProperty(["hasAvailableSlideDeck"], lesson) === true
    && getProperty(["slideDeck", "media", "media", "originalFile"], lesson) != null) {
    return getProperty(["slideDeck", "media", "media", "originalFile", "url"], lesson, false);
  }

  return false;
}

chrome.runtime.onMessage.addListener((msg, sender) => {
  // First, validate the message's structure
  if ((msg.from === 'content') && (msg.action === 'showPageAction')) {
    // Enable the page-action for the requesting tab
    chrome.pageAction.show(sender.tab.id);
  }
});

chrome.runtime.onConnect.addListener((port) => {
  console.log("Connected .....");

  // Listen to message from pop-up
  port.onMessage.addListener((msg) => {

    // We need to get some data from chrome storage -- this is async
    chrome.storage.local.get({
      enableXml: true,
      enableJson: false,
      defaultOwner: '',
      defaultCategory: '31887141',
      conversionProfileId: '',
      kalturaPartnerId: '',
      kalturaSecret: ''
    }, (items) => {
      // Add default category
      let defaultCategory = String(items.defaultCategory).trim();
      if (defaultCategory != "") {
        msg.categories.push(defaultCategory);
      }

      msg.toDownload.forEach((downloadable) => {
        let lessonId = getVideoFileId(downloadable, time());
        let folderName = getFolderName(downloadable, msg.courseId).replace(/[^\w\s-]/gi, "");
        console.log("Downloading " + lessonId);

        chrome.downloads.download({
          url: getPrimaryDownloadLink(downloadable, msg.downloadHD),
          filename: "Echo360_Lectures/" + folderName + "/1_" + lessonId + ".mp4"
        }, (downloadId) => {
          let secondaryVideoUrl = getSecondaryDownloadLink(downloadable, msg.downloadHD);
          if (secondaryVideoUrl !== false) {
            chrome.downloads.download({
              url: secondaryVideoUrl,
              filename: "Echo360_Lectures/" + folderName + "/2_" + lessonId + ".mp4"
            });
          }

          // Get ppt if it exists
          let presentationUrl = getPresentationDownloadLink(downloadable);
          if (presentationUrl) {
            chrome.downloads.download({
              url: presentationUrl,
              filename: "Echo360_Lectures/" + folderName + "/" + lessonId + '.' + getProperty(["lesson", "slideDeck", "media", "media", "originalFile", "ext"], downloadable, 'ppt')
            });
          }

          // For Debugging, can download JSON file from Echo360
          if (items.enableJson) {
            const jsonData = JSON.stringify(downloadable);
            console.log("Downloading JSON for " + lessonId);

            const jsonBlob = new Blob([jsonData], { type: "application/json" });
            chrome.downloads.download({
              url: URL.createObjectURL(jsonBlob), // The object URL can be used as download URL
              filename: "Echo360_Lectures/" + folderName + "/" + lessonId + ".json"
            });
          }

          // Only generate XML if we have the necessary data
          if (items.enableXml) {
            // use default owner if one wasn't provided
            let owner = (msg.ownerID != "") ? msg.ownerID : items.defaultOwner;

            // only generate KS if there is a secret, partnerid, and owner.
            let ks = (items.kalturaSecret != "" && items.kalturaPartnerId != "" && owner != "") ? generateKalturaSession(
              items.kalturaSecret,
              msg.ownerID,
              0, // User Type (admin is 2)
              items.kalturaPartnerId,
              86400 * 3, // 1 year
              "actionslimit:1,disableentitlement" // limit to 1 use
            ) : "";

            // Get XML data for the file
            const xmlData = getXML(
              downloadable,
              lessonId,
              owner,
              msg.coEditors,
              msg.coPublishers,
              msg.categories,
              msg.additionalTags.slice(0), // send a clone so they don't stack,
              secondaryVideoUrl,
              presentationUrl,
              String(items.conversionProfileId).trim(),
              ks
            );

            // Download XML DATA
            console.log("Downloading XML for " + lessonId);
            const xmlBlob = new Blob([xmlData], { type: "application/xml" });
            chrome.downloads.download({
              url: URL.createObjectURL(xmlBlob), // The object URL can be used as download URL
              filename: "Echo360_Lectures/" + folderName + "/" + lessonId + ".xml"
            });
          }
        }
        );
      });
    });
  });
});
