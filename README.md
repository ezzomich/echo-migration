# EchoMigration for Echo360 to Kaltura

It's your data: take it back.

## Introduction

This [Chrome Extension](https://support.google.com/chrome_webstore/answer/2664769?hl=en) facilitates extracting data from Echo360 for the purpose of migrating to Kaltura.

## TODO
- [x] Figure out how to generate XML
- [x] Handle Dual Screen Video
- [x] Update GUI
- [x] Specify download path
- [x] Name files downloaded and Map Tags
- [ ] Handle Caption files
- [x] Figure out how to access original slideshows?
- [ ] Get rid of JQuery
- [ ] Fix download blocking (multiple downloads, slow internet)

## Usage

### 1. Install the EchoMigration Chrome Extension.

### 2. Load the Echo page (via the link), click on the Gecko extension.
- Click on load Lectures.
- Select Lectures to download.
- Click on download.

<img src="/screenshots/screenshot-usecase.png" >

### 3. Downloading lectures.
<img src="/screenshots/screenshot-downloading.png" >

### 4. On Disk.
<img src="/screenshots/screenshot-datastore.png" >

## Known Issues
- Downloading many files in a short time span sometimes leads to Echo360 or Amazon S3 blocking downloads (sometimes it greys out lectures).

<img src="/screenshots/screenshot-blocked.png" >
